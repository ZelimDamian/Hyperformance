﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvestorNameArray : MonoBehaviour {

    public string[] Invname;
    public GameObject Prefab;
    public GameObject Canvas;

    public float step = 0.05f;

    void Start () {

        //Invoke("GenerateButtons", 1.0f);
    }

    string sceneName;

    public void GenerateButtons()
    {
        investorScene = new Dictionary<string, string>();
        var names = InvestorNamesService.Instance.Names;
        foreach (var name in names)
        {
            investorScene[name] = name + "Scene";
        }

        var Invname = InvestorNamesService.Instance.Names;

        for (int i = 0; i < Invname.Count; i++)
        {
            GameObject newButton = Instantiate(Prefab);
            newButton.transform.SetParent(Canvas.transform, false);
            newButton.transform.localPosition = new Vector3(0.0f, -i * step, 0.0f);

            var nameLabel = newButton.GetComponent<ListItem>();
            nameLabel.textMesh.text = Invname[i];

            nameLabel.ListItemPicked += (string name) => {
                sceneName = name;
                Invoke("LoadScene", 1.0f) ;
            };
        }
    }

    public Dictionary<string, string> investorScene;

    public void LoadScene()
    {
        Application.LoadLevel(investorScene[sceneName]);
    }
}
