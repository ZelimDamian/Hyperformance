﻿using System;
using System.Net;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO;

public class ApiService: Service {

    public static readonly ApiService Instance = new ApiService();

    //With the @ before the string, we can split a long string in many lines without getting errors
    private string json = @"{
		'hello':'world', 
		'foo':'bar', 
		'count':25
	}";

    string URL = "http://192.168.43.94:8080/api/";


    protected override void Init()
    {
    }

    

    public WWW Get(string endpoint)
    {
        WWW www = new WWW(URL + endpoint);
        return www;
    }

    public string Post(string endpoint, string json)
    {
        //CoroutineService.Instance.Runner.StartCoroutine(PostAsync(endpoint, data));

        var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL + endpoint);
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            //string json = "{\"user\":\"test\"," +
            //              "\"password\":\"bla\"}";

            streamWriter.Write(json);
            streamWriter.Flush();
            streamWriter.Close();
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            return streamReader.ReadToEnd();
        }
    }


    public IEnumerator PostAsync(string endpoint, string data)
    {
        ////Our custom Headers
        //Dictionary<string, string> headers = new Dictionary<string, string>();

        //headers.Add("Content-Type", "application/json");
        //headers.Add("Content-Length", json.Length.ToString());
        ////Replace single ' for double " 
        ////This is usefull if we have a big json object, is more easy to replace in another editor the double quote by singles one
        //json = json.Replace("'", "\"");
        ////Encode the JSON string into a bytes
        //byte[] postData = System.Text.Encoding.UTF8.GetBytes(json);
        ////Now we call a new WWW request
        //WWW www = new WWW(URL, postData, headers);
        ////And we start a new co routine in Unity and wait for the response.
        //return www;

        using (UnityWebRequest www = UnityWebRequest.Post(URL + endpoint, data))
        {
            www.SetRequestHeader("Content-Type", "application/json");

            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }

        //UnityWebRequest requestU = new UnityWebRequest(URL + endpoint, UnityWebRequest.kHttpVerbPOST);
        //byte[] bytes = System.Text.Encoding.UTF8.GetBytes(json);
        //UploadHandlerRaw uH = new UploadHandlerRaw(bytes);
        //requestU.SetRequestHeader("Content-Type", "application/json");
        //requestU.uploadHandler = uH;
        ////requestU.downloadHandler = ;
        //yield return requestU.Send();

        //if (requestU.isNetworkError || requestU.isHttpError)
        //{
        //    Debug.Log(requestU.error);
        //}
        //else
        //{
        //    Debug.Log("Form upload complete!");
        //}


    }

    //Wait for the www Request
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            //Print server response
            Debug.Log(www.text);
        }
        else
        {
            //Something goes wrong, print the error response
            Debug.Log(www.error);
        }
    }
}
