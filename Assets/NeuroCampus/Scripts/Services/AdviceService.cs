﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class Advice {

	public int index;
	public string name;
	public string text;
	public float time;
}

public class Advices {
	public Advice[] advices;
}

public class AdviceService : Service {

	public static readonly AdviceService Instance = new AdviceService();

	const string path = "Assets/Resources/advices.json";

	List<Advice> advices;

	protected override void Init() {

		using (StreamReader r = new StreamReader(path))
		{
			string json = r.ReadToEnd();
			var advices = CreateFromJSON (json);
			this.advices = new List<Advice>(advices.advices);
		}

		QuestionsService.Instance.OnAnswerPicked += delegate(Answer answer) {
			//if (Random.Range (0.0f, 1.0f) > 0.5f)
			{
				try {
					this.OnShowAdvice (advices [adviceIndex++]);
					accumulator = 0.0f;
				} catch {
				}
			}
		};
	}

	public float accumulator = 0.0f;
	int adviceIndex = 0;

	protected override void FixedUpdate() {
//		if (accumulator > 100.0f) {
//			try {
//				this.OnShowAdvice (advices [adviceIndex++]);
//				accumulator = 0.0f;
//			} catch { }
//		} else {
//			accumulator += Random.Range (0.0f, 0.01f);
//		}
	}

	public static Advices CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<Advices>(jsonString);
	}

	int currentAdviceIndex = -1;

	public Advice CurrentAdvice {
		get {
			try {
				return advices [currentAdviceIndex]; 
			} catch {
				return null;
			}
		}
	}



	public delegate void OnShowAdviceDelegate(Advice advice);
	public event OnShowAdviceDelegate OnShowAdvice;
}
