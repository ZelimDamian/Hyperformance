﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class ModelOutput {
	public ModelOutput(float attraction) {
		this.attraction = attraction;
	}

	public float attraction; // score difference
}

public class ModelService : Service {

	public static readonly ModelService Instance = new ModelService();

	public ModelOutput Calculate() {
		var sources = SourcesService.Instance.Inputs;
		return sources.Aggregate (new ModelOutput(0.0f), (acc, source) => {
			return new ModelOutput(acc.attraction + source.Diff.attraction);
		});
	}
}
