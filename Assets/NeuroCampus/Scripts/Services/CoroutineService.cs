﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineService : Service {

    public static readonly CoroutineService Instance = new CoroutineService();

    CoroutineRunner runner;
    public CoroutineRunner Runner
    {
        get
        {
            if (runner == null)
            {
                this.runner = GameObject.Find("OVRCameraRig").GetComponent<CoroutineRunner>();
            }
            return runner;
        }
    }
}
