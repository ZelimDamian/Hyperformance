﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvestorService: Service {

	public static readonly InvestorService Instance = new InvestorService();

	float attraction;

	public void ApplyScore(float score) {
		this.attraction += score;
	}

    public void ApplyAveragedScore(float score)
    {
        int sectionCount = QuestionsService.Instance.Questions.Count;
        this.attraction -= this.attraction / sectionCount;
        this.attraction += score / sectionCount;
    }

    public float Attraction {
		get { return attraction; }
	}

	protected override void Init() {
		this.attraction = Random.Range(40.0f, 45.0f);
	}
}
