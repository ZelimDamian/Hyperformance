﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SectionStateService : Service
{

    public static readonly SectionStateService Instance = new SectionStateService();

    public List<Dictionary<string, float>> State = new List<Dictionary<string, float>>();

    protected override void Init()
    {
        QuestionsService.Instance.OnAnswerPicked += (answer) => { CollectState(); };
    }

    void CollectState()
    {
        this.State.Add(new Dictionary<string, float>());
        foreach (var source in SourcesService.Instance.Inputs)
        {
            SaveState(source.Name, source.Average);
            source.Clear();
        }

        var session = State[State.Count - 1];

        var json = SendSection(session);
        var scoreText = ApiService.Instance.Post("trainers/trainer_id/predict", json);
        float score = float.Parse(scoreText) + 0.5f;

        InvestorService.Instance.ApplyAveragedScore(score * 100.0f);

        if (QuestionsService.Instance.IsLastQuestion)
        {
            Final();
        }
    }

    public void SaveState(string name, float value)
    {
        this.State[this.State.Count - 1][name] = value;
    }

    protected override void FixedUpdate()
    {
       
    }

    public void Final()
    {
        Debug.Log(this.State);
        SendSession();
    }

    //[System.Serializable]
    //struct Input
    //{

    //}

    //[System.Serializable]
    //struct Session
    //{
    //    string student_id = "";
    //    string trainer_id = "";
    //    Input[] inputs;
    //}

    public string SendSection(Dictionary<string, float>  section)
    {
        return "{ \n" + JsonConverterDict.ToJson(section) + "\n }";
    }

    public void SendSession()
    {
        var json = "{ \n \"sections\": [ \n";

        foreach (var section in State)
        {
            json += SendSection(section) + ",";
        }
        json = json.TrimEnd(',');

        json += " ] }";

        ApiService.Instance.Post("sessions", json);
    }

    private string _username;

    public string UserName
    {
        get { return _username; }
        set {
            _username = value;
        }
    }

    public delegate void OnStateCollectedDelegate();
    public event OnStateCollectedDelegate OnStateCollected;
}
