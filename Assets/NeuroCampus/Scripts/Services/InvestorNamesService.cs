﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Newtonsoft.Json;

public class InvestorNamesService : Service
{

    public static readonly InvestorNamesService Instance = new InvestorNamesService();

    public List<string> Names;

    [Serializable]
    public class Trainer
    {
        public string id;
        public string name;
    }

    [Serializable]
    public class Trainers
    {
        public Trainer[] trainers;
    }

    InvestorNamesService()
    {
        //CoroutineService.Instance.Runner.StartCoroutine(GetNames());
        //GetNames();

        Names = new List<string> { "Easy", "Hard" };
    }

    IEnumerator GetNames()
    {
        var request = ApiService.Instance.Get("trainers");

        while (!request.isDone)
        {
            Debug.Log("Download Stat: " + request.progress);

            //Wait each frame in each loop OR Unity would freeze
            yield return null;
        }

        if (string.IsNullOrEmpty(request.error))
        {
            string json = request.text.ToString();

            var trainers = JsonUtility.FromJson<Trainers>(json);

            Debug.Log(trainers.trainers.Length);

            //var dict = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
        }
    }
}
