﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SourcesService: Service {

	public static readonly SourcesService Instance = new SourcesService();

	public InputSource[] Inputs = {
		new MeditationSource(),
		new AttentionSource(),
		new OverdueSource(),
		new HandRangeSource(),
		new HandSpeedSource(),
		new AudienceGazeSource(),
		new BoardGazeSource(),
        new VoiceVolumeSource()
	};

	protected override void Init() {
		foreach (var input in Inputs) {
			input.Init ();
		}
	}

    protected override void FixedUpdate()
    {
        foreach (var input in Inputs)
        {
            input.FixedUpdate();
        }
    }
}
