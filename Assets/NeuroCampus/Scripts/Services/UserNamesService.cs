﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserNamesService : Service
{

    public static readonly UserNamesService Instance = new UserNamesService();

    public string[] Names;

    UserNamesService()
    {
        Names = new string[] {
            "Zelim",
            "Ruslan",
            "Ibrahim"
        };
    }
}
