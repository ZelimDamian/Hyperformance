﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class Answer {
	public int index;
	public string text;
	public int score;
}

[System.Serializable]
public class Question {
	
	public int index;
	public string text;
	public float time;

	public Answer[] answers;
}

[System.Serializable]
public class Questions {
	public Question[] questions;
}

public class QuestionsService: Service {

	const string path = "Assets/Resources/questions.json";

	public static readonly QuestionsService Instance = new QuestionsService();

	List<Question> questions;

	int currentQuestionIndex = 0;

	public Question CurrentQuestion {
		get { 
			try {
			return questions[currentQuestionIndex];
			} catch {
				return null;
			}
		}
	}

	public Answer GetAnswer(int index) {
		try {
			return CurrentQuestion.answers [index];
		} catch {
			return null;
		}
	}

	protected override void Init() {

		using (StreamReader r = new StreamReader(path))
		{
			string json = r.ReadToEnd();
			var questions = CreateFromJSON (json);
			this.questions = new List<Question>(questions.questions);
		}
	}

	public static Questions CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<Questions>(jsonString);
	}

	public List<Question> Questions { get { return questions; } }

	public void Pick(Question question, Answer answer) {

		InvestorService.Instance.ApplyScore (answer.score);
		this.GoToNextQuestion ();
		OnAnswerPicked (answer);
	}

	public void GoToNextQuestion() {
		this.currentQuestionIndex += 1;
		ExpectsAnswer = false;
		TimingService.Instance.SetCurrentQuestionStartedTime ();
	}

    public bool IsLastQuestion
    {
        get
        {
            return currentQuestionIndex >= this.questions.Count;
        }
    }

	public bool ExpectsAnswer = true;

	public delegate void OnAnswerPickedDelegate(Answer answer);
	public event OnAnswerPickedDelegate OnAnswerPicked;
}
