﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Service  {

	static List<Service> services = new List<Service>();

	public Service() {
	}

	public static void Register(Service service) {
		services.Add(service);
	}

	protected virtual void Init() { }
	protected virtual void FixedUpdate() { }

	public static void InitAll() {
		foreach (var service in services) {
			service.Init ();
		}
	}

	public static void FixedUpdateAll() {
		foreach (var service in services) {
			service.FixedUpdate ();
		}
	}
}
