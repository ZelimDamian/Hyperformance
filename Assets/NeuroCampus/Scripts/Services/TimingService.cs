﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimingService: Service {

	public static readonly TimingService Instance = new TimingService();

	public double time {
		get;
		set;
	}

	protected override void FixedUpdate ()
	{
		time = Time.time;
	}

	double currentQuestionStartedTime;

	public void SetCurrentQuestionStartedTime() {
		currentQuestionStartedTime = time;
	}

	public double TimeLeftForCurrentQuestion {
		get {
            try
            {
                return QuestionsService.Instance.CurrentQuestion.time - (time - currentQuestionStartedTime);
            }
            catch
            {
                return 0.0f;
            }
        }
	}
}
