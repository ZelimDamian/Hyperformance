﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceVolumeSource : InputSource {

    public override string Name
    {
        get
        {
            return "voice_volume";
        }
    }

    public override void Init() {
		InitMic ();
	}

	public override ModelOutput Diff {
		get {
			var volume = LevelMax () * this.Coefficient;
            history.Add(volume);
            return new ModelOutput (volume);
		}
	}

	public override float Coefficient {
		get {	
			return 100.0f;
		}
	}

	public static float MicLoudness;

	private string _device;

	//mic initialization
	void InitMic(){
		if(_device == null) _device = Microphone.devices[0];
		_clipRecord = Microphone.Start(_device, true, 999, 44100);
	}

	void StopMicrophone()
	{
		Microphone.End(_device);
	}

	AudioClip _clipRecord = new AudioClip();
	int _sampleWindow = 128;

	//get data from microphone into audioclip
	float  LevelMax()
	{
		float levelMax = 0;
		float[] waveData = new float[_sampleWindow];
		int micPosition = Microphone.GetPosition(null)-(_sampleWindow+1); // null means the first microphone
		if (micPosition < 0) return 0;
		_clipRecord.GetData(waveData, micPosition);
		// Getting a peak on the last 128 samples
		for (int i = 0; i < _sampleWindow; i++) {
			float wavePeak = waveData[i] * waveData[i];
			if (levelMax < wavePeak) {
				levelMax = wavePeak;
			}
		}
		return levelMax;
	}

}
