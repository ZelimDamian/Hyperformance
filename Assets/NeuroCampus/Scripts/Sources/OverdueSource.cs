﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverdueSource : InputSource {


    public override string Name
    {
        get
        {
            return "time_balance";
        }
    }

    public override void Init() {
		
	}

	public override ModelOutput Diff {
		get {
			float timeLeft = (float)TimingService.Instance.TimeLeftForCurrentQuestion;
            try
            {
                history.Add(timeLeft / QuestionsService.Instance.CurrentQuestion.time);
            } catch
            {

            }

            float timeOverdue = timeLeft > 0.0f ? 0.0f : timeLeft;
			return new ModelOutput (timeOverdue * this.Coefficient);
		}
	}

	public override float Coefficient {
		get {	
			return 0.1f;
		}
	}
}
