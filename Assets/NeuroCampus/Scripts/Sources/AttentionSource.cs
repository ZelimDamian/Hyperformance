﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttentionSource : InputSource {


    public override string Name
    {
        get
        {
            return "attention";
        }
    }

    //TGCConnectionController controller;

    public override void Init() {
		//controller = GameObject.Find("NeuroSkyTGCController").GetComponent<TGCConnectionController>();

		//controller.UpdateAttentionEvent += (int value) => {
		//	if (value != 0) {
		//		this.score = value - 50;
		//	} else {
		//		this.score = 0;
		//	}
		//	Debug.Log("Meditation: " + value);
		//};

	}

	public override ModelOutput Diff {
		get {
            history.Add(this.score);
            return new ModelOutput (this.score * this.Coefficient);
		}
	}

	public override float Coefficient {
		get {
			return 0.01f;
		}
	}
}
