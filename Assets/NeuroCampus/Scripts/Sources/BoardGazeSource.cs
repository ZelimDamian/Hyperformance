﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardGazeSource : InputSource
{

    public override string Name
    {
        get
        {
            return "board_gaze";
        }
    }

    GameObject target;


    public override void Init()
    {
        target = GameObject.Find("BoardGazeTarget");
    }

    float Measure()
    {
        Vector3 eyeToTarget = Vector3.Normalize(target.transform.position - Camera.main.transform.position);
        Vector3 gazeDirection = Camera.main.transform.forward;
        float angle = (float)Vector3.Angle(eyeToTarget, gazeDirection) / 90.0f;
        angle = Mathf.Clamp(angle, 0.0f, 0.5f);
        return angle;
    }

    public override ModelOutput Diff
    {
        get
        {
            var angle = Measure();
            history.Add(angle);
            return new ModelOutput(angle * this.Coefficient);
        }
    }

    public override float Average
    {
        get
        {
            return this.history.Average();
        }
    }

    public override float Coefficient
    {
        get
        {
            return 0.01f;
        }
    }

}
