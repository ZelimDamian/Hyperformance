﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class InputSource {

    protected InputSource()
    {
    }
    
    protected float score = 0;

	public abstract float Coefficient { get; }

    protected List<float> history = new List<float>();
    public virtual float Average
    {
        get
        {
            return history.Sum() / history.Count;
        }
    }

    public virtual void Clear()
    {
        this.history.Clear();
    }

    public abstract ModelOutput Diff { get; }

	public abstract void Init();

    public abstract string Name { get; }

    public virtual void FixedUpdate() { }

}
