﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandSpeedSource : InputSource
{

    public override string Name
    {
        get
        {
            return "hand_speed";
        }
    }

    GameObject rightHand, leftHand;
    Vector3 rightOldPos, leftOldPos;

    public override void Init()
    {
        rightHand = GameObject.Find("hand_right");
        leftHand = GameObject.Find("hand_left");

        Measure(); // warmup
    }

    public override void FixedUpdate()
    {
        var speed = Measure();
        history.Add(speed);
    }

    float Measure()
    {
        var leftPos = leftHand.transform.position;
        var rightPos = rightHand.transform.position;
        var leftSpeed = Vector3.Magnitude(leftPos - leftOldPos) / Time.fixedDeltaTime;
        var rightSpeed = Vector3.Magnitude(rightPos - rightOldPos) / Time.fixedDeltaTime;
        leftOldPos = leftPos;
        rightOldPos = rightPos;

        return (leftSpeed + rightSpeed) / 2.0f;
    }

    public override ModelOutput Diff
    {
        get
        {
            var speed = Measure();
            history.Add(speed);
            return new ModelOutput(speed * this.Coefficient);
        }
    }

    public override float Average
    {
        get
        {
            return this.history.Average();
        }
    }

    public override float Coefficient
    {
        get
        {
            return 0.01f;
        }
    }

}
