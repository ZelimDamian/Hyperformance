﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandRangeSource : InputSource
{

    public override string Name
    {
        get
        {
            return "hand_range";
        }
    }

    GameObject rightHand, leftHand;


    public override void Init()
    {
        rightHand = GameObject.Find("hand_right");
        leftHand = GameObject.Find("hand_left");
    }

    float Measure()
    {
        return Vector3.Magnitude(leftHand.transform.position - rightHand.transform.position);
    }

    public override ModelOutput Diff
    {
        get
        {
            var range = Measure();
            history.Add(range);
            return new ModelOutput(range * this.Coefficient);
        }
    }

    public override float Average
    {
        get
        {
            return this.history.Max();
        }
    }

    public override float Coefficient
    {
        get
        {
            return 0.01f;
        }
    }

}
