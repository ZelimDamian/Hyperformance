﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeditationSource : InputSource {

    //TGCConnectionController controller;

    public override string Name
    {
        get
        {
            return "meditation";
        }
    }

    public override void Init() {
		//controller = GameObject.Find("NeuroSkyTGCController").GetComponent<TGCConnectionController>();

		//score = 0.0f;

  //      controller.UpdateMeditationEvent += (int value) =>
  //      {
  //          if (value != 0)
  //          {
  //              this.score = value - 60;
  //          }
  //          else
  //          {
  //              this.score = 0;
  //          }
  //          Debug.Log("Meditation: " + value);
  //      };
    }

	public override ModelOutput Diff {
		get {
            history.Add(this.score / 100.0f);
			return new ModelOutput (this.score * this.Coefficient);
		}
	}

	public override float Coefficient {
		get {	
			return 0.01f;
		}
	}
}
