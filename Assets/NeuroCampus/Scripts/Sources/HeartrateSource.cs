﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartrateSource : InputSource {

    public override string Name
    {
        get
        {
            return "heartrate";
        }
    }

    public override void Init() {
		
	}

	public override ModelOutput Diff {
		get {
			return new ModelOutput (this.score * this.Coefficient);
		}
	}

	public override float Coefficient {
		get {
			return 0.1f;
		}
	}
}
