﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceGazeSource : InputSource
{

    public override string Name
    {
        get
        {
            return "eye_contact";
        }
    }

    GameObject target;


    public override void Init()
    {
        target = GameObject.Find("DesiredGazeTarget");
    }

    float Measure()
    {
        Vector3 eyeToTarget = Vector3.Normalize(target.transform.position - Camera.main.transform.position);
        Vector3 gazeDirection = Camera.main.transform.forward;
        double angle = 1.0f - Vector3.Angle(eyeToTarget, gazeDirection) / 90.0f;
        return (float) angle;
    }

    public override ModelOutput Diff
    {
        get
        {
            var angle = Measure();
            history.Add(angle);
            return new ModelOutput(angle * this.Coefficient);
        }
    }

    public override float Average
    {
        get
        {
            return this.history.Average();
        }
    }

    public override float Coefficient
    {
        get
        {
            return 0.01f;
        }
    }

}
