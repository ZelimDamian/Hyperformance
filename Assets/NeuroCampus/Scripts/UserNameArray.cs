﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserNameArray : MonoBehaviour {

    public string[] Invname;
    public GameObject Prefab;
    public GameObject Canvas;


    public float step = 0.05f;

    void Start() {
        GenerateButtons();
    }

    List<GameObject> buttons = new List<GameObject>();

    void GenerateButtons()
    {
        string[] Invname = UserNamesService.Instance.Names;

        for (int i = 0; i < Invname.Length; i++)
        {
            GameObject newButton = Instantiate(Prefab);
            newButton.transform.SetParent(Canvas.transform, false);
            newButton.transform.localPosition = new Vector3(0.0f, -i * step, 0.0f);

            buttons.Add(newButton);

            var nameLabel = newButton.GetComponent<ListItem>();
            nameLabel.textMesh.text = Invname[i];

            nameLabel.ListItemPicked += (string name) => {
                SelectUserName(name);
                nameLabel.enabled = false;

                foreach (var button in buttons)
                {
                    Destroy(button);
                }   

                Invoke("InitializeInvestorSelect", 1.0f);
            };
        }
    }

    void InitializeInvestorSelect()
    {
        var investorSelect = GetComponent<InvestorNameArray>();
        investorSelect.GenerateButtons();
    }

    void SelectUserName(string username)
    {
        SectionStateService.Instance.UserName = username;
    }
}
