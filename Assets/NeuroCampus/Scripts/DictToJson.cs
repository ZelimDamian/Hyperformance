﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

class JsonConverterDict
{
    //Dictionary<string, int> _dictionary = new Dictionary<string, int>()
    //{
    //    {"salmon", 5},
    //    {"tuna", 6},
    //    {"clam", 2},
    //    {"asparagus", 3}
    //};

    //public  ConvertDictionary()
    //{
    //    // Convert dictionary to string and save
    //    string s = GetLine(_dictionary);
    //    File.WriteAllText("dict.txt", s);
    //    // Get dictionary from that file
    //    Dictionary<string, int> d = GetDict("dict.txt");
    //}

    public static string ToJson(List<Dictionary<string, float>> d)
    {
        string json = "[ ";
        for (int i = 0; i < d.Count; i++)
        {
            json += "{ " + ToJson(d[i]) + " }, \n";
        }
        json = json.TrimEnd(',');
        json += " ]";
        return json;
    }

    public static string ToJson(Dictionary<string, float> d)
    {
        // Build up each line one-by-one and then trim the end
        StringBuilder builder = new StringBuilder();
        foreach (var pair in d)
        {
            builder.Append("\"" + pair.Key + "\"").Append(":").Append(pair.Value).Append(',');
        }
        string result = builder.ToString();
        // Remove the final delimiter
        result = result.TrimEnd(',');
        return result;
    }

    public static Dictionary<string, float> FromJson(string f)
    {
        var d = new Dictionary<string, float>();
        string s = File.ReadAllText(f);
        // Divide all pairs (remove empty strings)
        string[] tokens = s.Split(new char[] { ':', ',' },
            StringSplitOptions.RemoveEmptyEntries);
        // Walk through each item
        for (int i = 0; i < tokens.Length; i += 2)
        {
            string name = tokens[i];
            string freq = tokens[i + 1];

            // Fill the value in the sorted dictionary
            if (d.ContainsKey(name))
            {
                d[name] += float.Parse(freq);
            }
            else
            {
                d.Add(name, float.Parse(freq));
            }
        }
        return d;
    }
}