﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTextComponent : MonoBehaviour {

	TextMesh textMesh;

	public double Time {
		get { return TimingService.Instance.TimeLeftForCurrentQuestion; }
	}

	// Use this for initialization
	void Start () {
		textMesh = GetComponent<TextMesh> ();
	}

	// Update is called once per frame
	void Update () {
		this.textMesh.text = Time.ToString("F1");	
	}
}
