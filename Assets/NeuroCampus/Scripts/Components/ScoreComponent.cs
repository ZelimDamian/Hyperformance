﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreComponent : MonoBehaviour {

	TextMesh textMesh;

	public float Score {
		get { return InvestorService.Instance.Attraction; }
	}

	// Use this for initialization
	void Start () {
		textMesh = GetComponent<TextMesh> ();
	}

	// Update is called once per frame
	void Update () {
		this.textMesh.text = Score.ToString("F1");	
	}
}
