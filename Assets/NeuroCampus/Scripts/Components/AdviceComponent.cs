using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdviceComponent : MonoBehaviour {

	public bool IsPlaying {
		get { return GetComponent<Animation>().isPlaying; }
	}

	TextMesh textMesh;

	// Use this for initialization
	void Start () {
		textMesh = transform.Find ("Text").GetComponent<TextMesh> ();
		AdviceService.Instance.OnShowAdvice += delegate(Advice advice) {
			var animation = GetComponent<Animation> ();
			animation.PlayQueued("AdviceAnimation");
		};


	}
	
	// Update is called once per frame
	void Update () {
		var advice = AdviceService.Instance.CurrentAdvice;

		if (advice != null) {
			this.textMesh.text = advice.text;
		}
	}
}
