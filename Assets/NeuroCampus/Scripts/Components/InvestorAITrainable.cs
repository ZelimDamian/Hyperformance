﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvestorAITrainable : MonoBehaviour {

	public DifficultyLevel difficulty = DifficultyLevel.Easy;

	// Use this for initialization
	void Start () {

		Service.Register (InvestorService.Instance);
		Service.Register (ModelService.Instance);
		Service.Register (QuestionsService.Instance);
		Service.Register (SourcesService.Instance);
		Service.Register (TimingService.Instance);
		Service.Register (AdviceService.Instance);
        Service.Register (SectionStateService.Instance);
        Service.Register (ApiService.Instance);
        Service.Register (CoroutineService.Instance);

        InitializeInvestorState();

		Service.InitAll ();
	}

	void Update() {
		Animator animator;
		if(animator = GetComponent<Animator> ()) {
			animator.SetFloat ("Blend", InvestorService.Instance.Attraction);	
		}
	}

	void FixedUpdate() {
		Service.FixedUpdateAll ();
	}

	void InitializeInvestorState () {
		InvokeRepeating("RunStateService", 3.0f, 0.5f);
	}

	void RunStateService() {
        ModelService.Instance.Calculate();

    }
}
