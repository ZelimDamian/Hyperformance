﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ListItem : MonoBehaviour
{
    public TextMesh textMesh;
    public int Index;
    Collider collider;
    LineRenderer lineRenderer;

    // Use this for initialization
    void Start()
    {
        hand = GameObject.Find("hand_right").GetComponent<Collider>();
        collider = GetComponent<Collider>();

        layerMask = LayerMask.NameToLayer("Everything");
        lineRenderer = GetComponent<LineRenderer>();

        
    }


    public LayerMask layerMask;
    public float GrabRadius = 0.001f;

   

// called when user picks this question
public void Pick()
    {
        ListItemPicked(textMesh.text);
    }

    void ResetExpectingAnswer()
    {
        QuestionsService.Instance.ExpectsAnswer = true;
    }

    void OnMouseDown()
    {
        Pick();
    }

    Collider hand;

    // Update is called once per frame
    void Update()
    {

        DoGrabbing();

        if (Input.GetKeyDown("" + Index))
        {
            Pick();
        }
    }

    void DoGrabbing()
    {
        var center = this.hand.transform.position;
        lineRenderer.SetPosition(0, center);
        lineRenderer.SetPosition(1, center + hand.transform.forward * 10.0f);

        if (QuestionsService.Instance.ExpectsAnswer)
        {
            RaycastHit[] hits = Physics.RaycastAll(new Ray(hand.transform.position, hand.transform.forward), 10.0f, layerMask);
            if (hits.Length > 0)
            {
                var handHit = hits.Any(hit => hit.collider == collider);

                if (handHit)
                {
                    this.gameObject.GetComponent<Renderer>().material.color = Color.green;
                    var indexTriggerDown = OVRInput.Get(OVRInput.RawButton.RIndexTrigger);

                    lineRenderer.SetPosition(1, this.hand.transform.position);

                    if (indexTriggerDown)
                    {
                        Pick();
                    }
                }
                else
                {
                    this.gameObject.GetComponent<Renderer>().material.color = Color.white;
                }
            }
            else
            {
                this.gameObject.GetComponent<Renderer>().material.color = Color.white;
            }
        }
    }

    public bool Hovered = false;


    public bool WrapOnStart;
    public float WrapWidth;

    public void Wrap(TextMesh tm, float MaxWidth)
    {
        if (tm == null)
        {
            Debug.LogError("TextMesh component not found.");
            return;
        }

        Font f = tm.font;
        string str = tm.text;
        int nLastWordInd = 0;
        int nIter = 0;
        float fCurWidth = 0.0f;
        float fCurWordWidth = 0.0f;

        while (nIter < str.Length)
        {
            char c = str[nIter];

            if (c == '\n')
            {
                nLastWordInd = nIter;
                fCurWidth = 0.0f;
            }
            else
            {
                if (c == ' ')
                {
                    nLastWordInd = nIter; // replace this character with '/n' if breaking here
                    fCurWordWidth = 0.0f;
                }

                fCurWidth += 1.0f;
                fCurWordWidth += 1.0f;
                if (fCurWidth >= MaxWidth)
                {
                    str = str.Remove(nLastWordInd, 1);
                    str = str.Insert(nLastWordInd, "\n");
                    fCurWidth = fCurWordWidth;
                }
            }

            ++nIter;
        }

        tm.text = str;
    }
    
    public delegate void ListItemPickedDelegate(string item);
    public event ListItemPickedDelegate ListItemPicked;
}
