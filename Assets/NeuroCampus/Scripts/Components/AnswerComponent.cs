﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AnswerComponent : MonoBehaviour {

	Question question;
	Answer answer { get { return QuestionsService.Instance.GetAnswer (AnswerIndex); } }

	public int AnswerIndex = 0;

	TextMesh textMesh;

	// Use this for initialization
	void Start () {
		textMesh = transform.Find ("AnswerText").GetComponent<TextMesh> ();
		hand = GameObject.Find ("hand_right").GetComponent<Collider>();

		var controller = OVRInput.GetActiveController ();

		layerMask = LayerMask.NameToLayer ("Everything");
	}
	
	// Update is called once per frame
	void UpdateText () {
		MeshRenderer renderer = gameObject.GetComponentInChildren<MeshRenderer>();

		if (answer != null) {
			textMesh.text = answer.text;	
			renderer.enabled = true;
		} else {
			textMesh.text = "";
			renderer.enabled = false;
		}

		Wrap (textMesh, 15.0f);
	}

	public LayerMask layerMask;
	public float GrabRadius = 0.1f; 

	// called when user picks this question
	public void Pick() {
		QuestionsService.Instance.Pick (question, answer);
		Invoke("ResetExpectingAnswer", 3.0f);
	}

	void ResetExpectingAnswer() {
		QuestionsService.Instance.ExpectsAnswer = true;
	}

	void OnMouseDown() {
		Pick ();
	}

	Collider hand;

	// Update is called once per frame
	void Update () {

		UpdateText ();

		DoGrabbing ();

		if (Input.GetKeyDown ("" + AnswerIndex) ) {
			Pick ();
		}
	}

	void DoGrabbing() {
		if (QuestionsService.Instance.ExpectsAnswer) {
			RaycastHit[] hits = Physics.SphereCastAll (transform.position, GrabRadius, transform.forward, 0.0f, layerMask);
			if (hits.Length > 0) {
				var handHit = hits.Any (hit => hit.collider == hand);

				if (handHit) {
					this.gameObject.GetComponent<Renderer>().material.color = Color.green;
					var indexTriggerDown = OVRInput.Get(OVRInput.RawButton.RIndexTrigger); 

					if (indexTriggerDown) {
						Pick ();	
					}
				} else {
					this.gameObject.GetComponent<Renderer>().material.color = Color.white;
				}
			}  else {
				this.gameObject.GetComponent<Renderer>().material.color = Color.white;
			}
		}
	}

	public bool Hovered = false;








	public bool WrapOnStart;
	public float WrapWidth;

	public void Wrap(TextMesh tm, float MaxWidth)
	{
		if (tm == null)
		{
			Debug.LogError("TextMesh component not found.");
			return;
		}

		Font f = tm.font;
		string str = tm.text;
		int nLastWordInd = 0;
		int nIter = 0;
		float fCurWidth = 0.0f;
		float fCurWordWidth = 0.0f;

		while (nIter < str.Length)
		{
			char c = str[nIter];

			if (c == '\n')
			{
				nLastWordInd = nIter;
				fCurWidth = 0.0f;
			}
			else
			{
				if (c == ' ')
				{
					nLastWordInd = nIter; // replace this character with '/n' if breaking here
					fCurWordWidth = 0.0f;
				}

				fCurWidth += 1.0f;
				fCurWordWidth += 1.0f;
				if (fCurWidth >= MaxWidth)
				{
					str = str.Remove(nLastWordInd, 1);
					str = str.Insert(nLastWordInd, "\n");
					fCurWidth = fCurWordWidth;
				}
			}

			++nIter;
		}

		tm.text = str;
	}

}
