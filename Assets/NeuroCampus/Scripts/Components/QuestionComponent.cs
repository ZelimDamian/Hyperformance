﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionComponent : MonoBehaviour {

	TextMesh textMesh;

	public int QuestionIndex {
		get { return QuestionsService.Instance.CurrentQuestion.index; }
	}

	// Use this for initialization
	void Start () {
		textMesh = GetComponent<TextMesh> ();
	}
	
	// Update is called once per frame
	void Update () {
		var question = QuestionsService.Instance.CurrentQuestion;
		if (question != null) {
			this.textMesh.text = question.text;	
		}
	}
}
