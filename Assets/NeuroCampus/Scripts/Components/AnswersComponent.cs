﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswersComponent : MonoBehaviour {


	// Use this for initialization
	void Start () {


		QuestionsService.Instance.OnAnswerPicked += delegate(Answer answer) {
			var animation = GetComponent<Animation> ();
			animation.PlayQueued("AnswersAnimation");
		};
	}

}
