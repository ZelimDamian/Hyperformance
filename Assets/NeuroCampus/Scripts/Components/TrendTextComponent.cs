﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrendTextComponent : MonoBehaviour {

	TextMesh textMesh;

	public string Text {
		get { return ModelService.Instance.Calculate().attraction.ToString("F1"); }
	}

	// Use this for initialization
	void Start () {
		textMesh = GetComponent<TextMesh> ();
	}

	// Update is called once per frame
	void Update () {
		this.textMesh.text = Text;	
	}
}
