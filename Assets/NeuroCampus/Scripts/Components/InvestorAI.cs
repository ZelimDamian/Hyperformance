﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DifficultyLevel {
	Easy, Normal, Hard, Exam
}

public class InvestorAI : MonoBehaviour {

	public DifficultyLevel difficulty = DifficultyLevel.Easy;

	// Use this for initialization
	void Start () {

        Service.Register (ApiService.Instance);
        Service.Register (InvestorService.Instance);
		Service.Register (ModelService.Instance);
		Service.Register (QuestionsService.Instance);
		Service.Register (SourcesService.Instance);
		Service.Register (TimingService.Instance);
		Service.Register (AdviceService.Instance);

        InitializeInvestorState ();

		Service.InitAll ();
	}

	void Update() {
		Animator animator;
		if(animator = GetComponent<Animator> ()) {
			animator.SetFloat ("Blend", InvestorService.Instance.Attraction);	
		}
	}

	void FixedUpdate() {
		Service.FixedUpdateAll ();
	}

	void InitializeInvestorState () {
		InvokeRepeating("RunModel", 3.0f, 0.5f);
	}

	void RunModel() {
		var reaction = ModelService.Instance.Calculate ();
		InvestorService.Instance.ApplyScore (reaction.attraction);
	}
}
