﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastF : MonoBehaviour {

    private void Update()
    {
        RaycastHit hit;

        Vector3 forward = transform.TransformDirection(Vector3.forward) * 100;
        Debug.DrawRay(transform.position, forward, Color.green);

        if(Physics.Raycast(transform.position,(forward), out hit))
        {
            print("Hit Successfull");
        }
    }
}
