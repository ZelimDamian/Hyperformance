﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetText : MonoBehaviour
{
    public string[] Questions;
    public GameObject Panel;
    public GameObject Prefab;

    void Start()
    {
        GenerateText();
    }

    List<GameObject> text = new List<GameObject>();

    void GenerateText()
    {
        var Questions = QuestionsService.Instance.Questions;

        for (int i = 0; i < Questions.Count; i++)
        {

            GameObject newText = Instantiate(Prefab);
            newText.transform.SetParent(Panel.transform, false);
            text.Add(newText);
            
        }
    }
}