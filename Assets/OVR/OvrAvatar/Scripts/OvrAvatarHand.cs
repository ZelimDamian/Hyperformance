﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class OvrAvatarHand : MonoBehaviour, IAvatarPart
{

    float alpha = 1.0f;

	public OvrAvatarDriver.ControllerPose Pose;

    public void UpdatePose(OvrAvatarDriver.ControllerPose pose)
    {
		this.Pose = pose;
    }

    public void SetAlpha(float alpha)
    {
        this.alpha = alpha;
    }

    public void OnAssetsLoaded()
    {
        SetAlpha(this.alpha);
    }

    public void RControllerRayCast()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, fwd, 10))
            print("There is something in front of the object!");
    }
}