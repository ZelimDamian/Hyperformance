﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class OvrAvatarTouchController : MonoBehaviour, IAvatarPart {

    float alpha = 1.0f;

    public void UpdatePose(OvrAvatarDriver.ControllerPose pose)
    {
    }

    public void SetAlpha(float alpha)
    {
        this.alpha = alpha;
    }

    public void OnAssetsLoaded()
    {
        SetAlpha(this.alpha);
    }

    public void RControllerRayCast()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, fwd, 100))
            print("There is something in front of the object!");
    }
}
